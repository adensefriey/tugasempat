import React, {useEffect} from 'react';
import { View, Text,Image} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.replace('Home')
        }, 3000)
    })
    return (
        <View style={{justifyContent:'center',alignItems:'center',height:650}}>
            <Text style={{fontSize:30,fontWeight:'bold'}}>HALO SUPRIYADI</Text>
            <Image source={require('../gambar/madura.png')}
            style={{width:200,height:200,}}></Image>
        </View>
    );
};

export default Splash;