import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Home, Splash,} from '../pages';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator
        screenOptions={{headerShown: false}}>
            <Stack.Screen name="Splash" component={Splash}/>          
            <Stack.Screen name="Home" component={Home}/>  
        </Stack.Navigator>
    )
}
export default Route